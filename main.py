import pickle
import wx
import gui
from secrets import token_bytes
from typing import Tuple
import base64

class decryptFrame(gui.decryptFrame):
    def __init__(self, parent):
        gui.decryptFrame.__init__(self, parent)
        self.PhotoMaxSize = 500

    def encrypted_imgOpen( self, event ):
        print("Encrypted Image Open")

    def but_decrypt( self, event ):
        print("But Decrypt")

        with open("key1.txt", "r") as k1, open("key2.txt", "r") as k2:
            key1: int = int(k1.read())
            key2: int = int(k2.read())
            result: str = self.decrypt(key1, key2)

            pic = base64.b64decode(result)
            filename = 'new_image.jpg'
            with open(filename, 'wb') as f:
                f.write(pic)
                #img = wx.Image(filename, wx.BITMAP_TYPE_ANY)
                #W = img.GetWidth()
                #H = img.GetHeight()
                #if W > H:
                #    NewW = self.PhotoMaxSize
                #    NewH = self.PhotoMaxSize * H / W
                #else:
                #    NewH = self.PhotoMaxSize
                #    NewW = self.PhotoMaxSize * W / H
                #self.crypt_image.SetBitmap(wx.Bitmap(wx.BitmapFromImage(img.Scale(NewW, NewH))))
                self.crypt_image.SetBitmap(filename)
        #except:
        #    print("Error: file not found")

    def decrypt(self, key1: int, key2: int) -> str:
        decrypted: int = key1 ^ key2  # XOR
        temp: bytes = decrypted.to_bytes((decrypted.bit_length() + 7) // 8, "big")
        return temp.decode()

    def open_encrypt( self, event ):
        print("Open Encrypt")
        encryptFrame(self).Show()
        self.Hide()




#open_decrypt
class encryptFrame(gui.encryptFrame):
    def __init__(self, parent):
        gui.encryptFrame.__init__(self, parent)
        self.current_image = ""
        #self.rand_key = ""

    def but_encrypt( self, event ):
        print("But Encrypt")
        with open(self.current_image, "rb") as imageFile:
            b_str = base64.b64encode(imageFile.read())
            img_str = b_str.decode("utf-8")
            print(len(img_str))

        key1, key2 = self.encrypt(img_str)  # giv det krypterede her
        print(key1)
        print(key2)
        with open("key1.txt", "w") as txt1, open("key2.txt", "w") as txt2:
            txt1.write(str(key1))
            txt2.write(str(key2))

    def open_decrypt( self, event ):
        print("Open Decrypt")
        decryptFrame(self).Show()
        self.Hide()

    def imgOpen( self, event ):
        print("Image Open")
        self.current_image = self.file_pick.GetPath()
        #print(self.current_image)



    def random_key(self, length: int) -> int:
        # generate length random bytes
        tb: bytes = token_bytes(length)
        # convert those bytes into a bit string and return it
        return int.from_bytes(tb, "big")

    def encrypt(self, original: str) -> Tuple[int, int]:
        original_bytes: bytes = original.encode()
        dummy: int = self.random_key(len(original_bytes))
        original_key: int = int.from_bytes(original_bytes, "big")
        # big betyder, at mest betydende bit er i starten (venstre), altså den bit, der repræsentere den højeste numeriske værdi
        encrypted: int = original_key ^ dummy  # XOR
        return dummy, encrypted



app = wx.App(False)
frame = encryptFrame(None)
frame.Show(True)
app.MainLoop()