from secrets import token_bytes
from typing import Tuple
import base64

def random_key(length: int) -> int:
    # generate length random bytes
    tb: bytes = token_bytes(length)
    # convert those bytes into a bit string and return it
    return int.from_bytes(tb, "big")

def encrypt(original: str) -> Tuple[int, int]:
    original_bytes: bytes = original.encode()
    dummy: int = random_key(len(original_bytes))
    original_key: int = int.from_bytes(original_bytes, "big")
    # big betyder, at mest betydende bit er i starten (venstre), altså den bit, der repræsentere den højeste numeriske værdi
    encrypted: int = original_key ^ dummy  # XOR
    return dummy, encrypted


if __name__ == "__main__":
    with open("dannebrog.png", "rb") as imageFile:
        b_str = base64.b64encode(imageFile.read())
        img_str = b_str.decode("utf-8")
        print(len(img_str))

    key1, key2 = encrypt(img_str) #giv det krypterede her
    print(key1)
    print(key2)
    with open("key1.txt", "w") as txt1, open("key2.txt", "w") as txt2:
        txt1.write(str(key1))
        txt2.write(str(key2))

    """
    result: str = decrypt(key1, key2)
    print(result)

    pic = base64.b64decode(result)
    filename = 'new_image.jpg'
    with open(filename, 'wb') as f:
        f.write(pic)
    """