# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class encryptFrame
###########################################################################

class encryptFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Billedkryptering", pos = wx.DefaultPosition, size = wx.Size( 300,200 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Vælg billede der skal krypteres:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		self.m_staticText1.SetFont( wx.Font( 10, 74, 90, 92, False, "Arial" ) )
		
		bSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.file_pick = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer1.Add( self.file_pick, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.button_encrypt = wx.Button( self, wx.ID_ANY, u"Krypter", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.button_encrypt.SetFont( wx.Font( 9, 74, 90, 92, False, "Arial" ) )
		
		bSizer1.Add( self.button_encrypt, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Dekrypter", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem2 )
		
		self.m_menubar1.Append( self.m_menu1, u"Fil" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.file_pick.Bind( wx.EVT_FILEPICKER_CHANGED, self.imgOpen )
		self.button_encrypt.Bind( wx.EVT_BUTTON, self.but_encrypt )
		self.Bind( wx.EVT_MENU, self.open_decrypt, id = self.m_menuItem2.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def imgOpen( self, event ):
		event.Skip()
	
	def but_encrypt( self, event ):
		event.Skip()
	
	def open_decrypt( self, event ):
		event.Skip()
	

###########################################################################
## Class decryptFrame
###########################################################################

class decryptFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Dekrypter", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Krypter", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem1 )
		
		self.m_menubar1.Append( self.m_menu1, u"Fil" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		gSizer1 = wx.GridSizer( 0, 2, 0, 0 )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Vælg fil der skal dekrypteres", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )
		self.m_staticText2.SetFont( wx.Font( 10, 74, 90, 92, False, "Arial" ) )
		
		bSizer3.Add( self.m_staticText2, 0, wx.ALL, 5 )
		
		self.m_filePicker2 = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer3.Add( self.m_filePicker2, 0, wx.ALL, 5 )
		
		self.button_decrypt = wx.Button( self, wx.ID_ANY, u"Dekrypter", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.button_decrypt, 0, wx.ALL, 5 )
		
		
		gSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )
		
		self.crypt_image = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.crypt_image, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		self.SetSizer( gSizer1 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_MENU, self.open_encrypt, id = self.m_menuItem1.GetId() )
		self.m_filePicker2.Bind( wx.EVT_FILEPICKER_CHANGED, self.encrypted_imgOpen )
		self.button_decrypt.Bind( wx.EVT_BUTTON, self.but_decrypt )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def open_encrypt( self, event ):
		event.Skip()
	
	def encrypted_imgOpen( self, event ):
		event.Skip()
	
	def but_decrypt( self, event ):
		event.Skip()
	

