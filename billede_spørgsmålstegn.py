class MainWindow(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self,None,-1,"Agent-Based Model of Residential Development", size = (APP_SIZE_X, APP_SIZE_Y))

        self.panel = wx.Panel(self,-1)
        self.imageFile = "GraphImage\\PlotPic1554451866.png"  # provide a diff file name in same directory/path
        self.bmp = wx.Image(self.imageFile,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        wx.StaticBitmap(self.panel, -1, self.bmp, (500,-20), (1000,1000))

        #button42 = wx.Button(self.panel, -1, "Read", pos=(100,800), size=(300,100))
        #self.Bind(wx.EVT_BUTTON, self.OnRead ,button42)

    def OnRead(self,event):
        self.imageFile1="GraphImage\\PlotPic1554215532.png" # you have to provide a diff image file name
        self.bmp = wx.Image(self.imageFile1,wx.BITMAP_TYPE_PNG).ConvertToBitmap()

        self.obj = wx.StaticBitmap(self.panel, -1, self.bmp, (500,-20),(1000,1000))
        self.obj.Refresh()