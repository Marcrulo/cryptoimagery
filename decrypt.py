from secrets import token_bytes
from typing import Tuple
import base64

def decrypt(key1: int, key2: int) -> str:
    decrypted: int = key1 ^ key2  # XOR
    temp: bytes = decrypted.to_bytes((decrypted.bit_length() + 7) // 8, "big")
    return temp.decode()

if __name__ == "__main__":
    try:
        with open("key1.txt", "r") as k1, open("key2.txt", "r") as k2:
            key1: int = int(k1.read())
            key2: int = int(k2.read())
            result: str = decrypt(key1, key2)

            pic = base64.b64decode(result)
            filename = 'new_image.jpg'
            with open(filename, 'wb') as f:
                f.write(pic)
    except:
        print("Error: file not found")